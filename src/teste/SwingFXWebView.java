/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import com.sun.javafx.application.PlatformImpl;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import javafx.embed.swing.JFXPanel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.lang.reflect.Field;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue; 
import javafx.concurrent.Worker;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle; 
import javax.swing.JButton;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
  
/** 
 * SwingFXWebView 
 */  
public class SwingFXWebView extends JPanel {  
    
    private class iniciaNavegador extends Thread{
        public void run(){
            System.out.println("opa");

            stage = new Stage();  

            stage.setTitle("Hello Java FX");  
            stage.setResizable(true);  

            //Group root = new Group();  
            webView = new WebView();
            webView.setPrefSize(w, h);
            webEngine = webView.getEngine();
            setupActions();
            Scene scene = new Scene(webView);
            scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
            stage.initStyle(StageStyle.TRANSPARENT);
            // Set up the embedded browser:
            //webEngine.documentProperty().addListener(new DocListener());
            webEngine.load(site);
            //webEngine.loadContent("<body style='background : rgba(0,0,0,0);font-size: 70px;text-align:center;'>Test Transparent</body>");
            //webEngine.load("http://localhost:8080/teste/teste.html");

            //ObservableList<Node> children = root.getChildren();
            //children.add(browser);                     

            jfxPanel.setScene(scene);
        }

        private void setupActions() {
            webEngine.getLoadWorker().stateProperty().addListener(
            new ChangeListener<Worker.State>() {
                public void changed(ObservableValue ov, Worker.State oldState, Worker.State newState) {
                    if (newState == Worker.State.SUCCEEDED) {
                        Document doc = webEngine.getDocument();
                        try {
                            Transformer transformer = TransformerFactory.newInstance().newTransformer();
                            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                            transformer.transform(new DOMSource(doc),
                                    new StreamResult(new OutputStreamWriter(System.out, "UTF-8")));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            });
        }
    }
     
    public Stage stage;  
    public WebView webView;  
    public JFXPanel jfxPanel;  
    public WebEngine webEngine;
    public String site;
    public int w = 580;
    public int h = 300;
    public JButton bot;
  
    public SwingFXWebView(){  
        //initComponents();  
    }  
     
    public void initComponents(String s, JButton bot){  
        this.bot = bot; 
        jfxPanel = new JFXPanel(); 
        jfxPanel.setSize(w, h);
        jfxPanel.setMinimumSize(new Dimension(w, h));
        jfxPanel.setOpaque(true);
        site = s;
        //jfxPanel.setBackground(java.awt.Color.red);
        createScene();  
         
        setLayout(new BorderLayout());  
        add(jfxPanel);           
    }     
     
    /** 
     * createScene 
     * 
     * Note: Key is that Scene needs to be created and run on "FX user thread" 
     *       NOT on the AWT-EventQueue Thread 
     * 
     */  
    
    private URI uri = null;
    public Map<String, List<String>> cookie = null;
    public CookieManagerWS cookieManager = null;
    private CookieStore cookieJar = null;
    
    private void createScene() {  
        //new iniciaNavegador().start();
        PlatformImpl.setImplicitExit(false);
        PlatformImpl.startup(new Runnable() {  
            @Override
            public void run() {  
                System.out.println("opa");
                loadCookie();
                loadCookieManager();

                try {
                    uri = new URI(site);
                } catch (URISyntaxException ex) {
                    ex.printStackTrace();
                }
                                
                stage = new Stage();  
                 
                stage.setTitle("Hello Java FX");  
                stage.setResizable(true);  
   
                //Group root = new Group();  
                webView = new WebView();
                webView.setPrefSize(w, h);
                webEngine = webView.getEngine();
                Scene scene = new Scene(webView);
                scene.setFill(Color.TRANSPARENT);
                stage.setScene(scene);
                stage.initStyle(StageStyle.TRANSPARENT);
                
                CookieManager cookieManager = new CookieManager();
                cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
                CookieHandler.setDefault(cookieManager);
                
                cookieJar =  cookieManager.getCookieStore();
                //cookieJar.add(uri, hc);

//                try {
//                    CookieHandler.getDefault().put(uri, cookie);
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }

                webEngine.load(site);
                jfxPanel.setScene(scene);
                if(bot != null){
                    bot.setEnabled(true);
                }
            }  
        });
    }
    
    private void setupActionsAndCookies(){
        
    }
    
    private void saveCookie(){
        FileOutputStream out = null;
        ObjectOutputStream objOut = null;
        try {
            out = new FileOutputStream("cookies.cok");
            objOut = new ObjectOutputStream(out);
            objOut.writeObject(cookie);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally{
            if(objOut != null) try { objOut.close(); } catch (IOException ex) {}
            if(out != null) try { out.close(); } catch (IOException ex){};
        }
    }
    
    private void prepareSaveCookieManager(){
        cookieManager = (CookieManagerWS) CookieHandler.getDefault();
        saveCookie();
    }
    
    private void saveCookieManager(){
        FileOutputStream out = null;
        ObjectOutputStream objOut = null;
        try {
            out = new FileOutputStream("cookiesmenager.cok");
            objOut = new ObjectOutputStream(out);
            objOut.writeObject(cookieManager);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally{
            if(objOut != null) try { objOut.close(); } catch (IOException ex) {}
            if(out != null) try { out.close(); } catch (IOException ex){};
        }
    }
    
    private void loadCookieManager(){
        FileInputStream in = null;
        ObjectInputStream objIn = null;
        try {
            in = new FileInputStream("cookiesmenager.cok");
            objIn = new ObjectInputStream(in);
            cookieManager = (CookieManagerWS) objIn.readObject();
        } catch (FileNotFoundException ex) {
            cookieManager = new CookieManagerWS();
            saveCookieManager();
            loadCookieManager();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }finally{
            if(in != null)try{ in.close(); } catch (IOException ex) {}
            if(objIn != null)try{ objIn.close(); } catch (IOException ex) {}
        }
    }
    
    private void loadCookie(){
        FileInputStream in = null;
        ObjectInputStream objIn = null;
        try {
            in = new FileInputStream("cookies.cok");
            objIn = new ObjectInputStream(in);
            cookie = (Map<String, List<String>>) objIn.readObject();
        } catch (FileNotFoundException ex) {
            cookie = new LinkedHashMap<>();
            saveCookie();
            loadCookie();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }finally{
            if(in != null)try{ in.close(); } catch (IOException ex) {}
            if(objIn != null)try{ objIn.close(); } catch (IOException ex) {}
        }
    }
    
    public void getTeste(){
        List <HttpCookie> cookies = cookieJar.getCookies();
        for (HttpCookie cookie: cookies) {
          System.out.println("CookieHandler retrieved cookie: " + cookie);
        }
    }
    
    public void printCookie(){
        System.out.println("-------------------------------------");
        for(Map.Entry<String, List<String>> e : cookie.entrySet()){
            System.out.println("key = "+e.getKey());
            for(String s : e.getValue()){
                System.out.println("value = "+s);
            }
            System.out.println("-------------------------------------");
        }
    }
    
    public void getCookie(){
        try {
            cookie = CookieHandler.getDefault().get(uri,cookie);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void getLoadCookie(){
        System.out.println("Load cookie");
        CookieHandler.setDefault(cookieManager);
    }
    
    public void getSaveCookie(){
        saveCookie();
        prepareSaveCookieManager();
    }
    
    public void getHTML(){
        PlatformImpl.setImplicitExit(false);
        PlatformImpl.startup(new Runnable() {  
            @Override
            public void run() {  
                String html = (String) webEngine.executeScript("document.documentElement.outerHTML");
                System.out.println(html);            
            }  
        });
    }
    
    class DocListener implements ChangeListener<Document>{
            @Override
            public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                try {

                    // Use reflection to retrieve the WebEngine's private 'page' field. 
                    Field f = webEngine.getClass().getDeclaredField("page"); 
                    f.setAccessible(true); 
                    com.sun.webkit.WebPage page = (com.sun.webkit.WebPage) f.get(webEngine);  
                    page.setBackgroundColor((new java.awt.Color(0, 0, 0, 0)).getRGB()); 

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }  
}
